﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;


namespace StageTools
{


    public struct FreezeState
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public FreezeState(Vector3 _position, Quaternion _rotation, Vector3 _scale)
        {
            position = _position;
            rotation = _rotation;
            scale = _scale;
        }
    }

    [InitializeOnLoadAttribute]
    public static class FreezableStageManager
    {

        static Dictionary<GameObject, FreezeState> states;

        public static GUIStyle guiStyle;

        private static bool _unfreezeOnPlay = false;

        public static bool unfreezeOnPlay
        {
            get
            {
                return _unfreezeOnPlay;
            }

            set
            {
                _unfreezeOnPlay = value;
                EditorPrefs.SetBool(unfreezeOnPlaySettingKey, value);
            }
        }

        public const string unfreezeOnPlaySettingKey = "StageTools.UnfreezeOnPlay";

        static FreezableStageManager()
        {
            EditorApplication.playModeStateChanged += HandlePlayModeStateChange;
            
            guiStyle = new GUIStyle();
            guiStyle.alignment = TextAnchor.MiddleCenter;
            guiStyle.normal.textColor = Color.white;
            guiStyle.fontSize = 24;            
            _unfreezeOnPlay = EditorPrefs.GetBool(unfreezeOnPlaySettingKey);
            EditorApplication.delayCall += () =>
            {
                Menu.SetChecked("Stage tools/Freezer/Unfreeze on play", unfreezeOnPlay);
            };
        }

        private static void HandlePlayModeStateChange(PlayModeStateChange state)
        {
            if(state == PlayModeStateChange.EnteredPlayMode && unfreezeOnPlay)
            {
                FreezableStageObject[] fsos = GameObject.FindObjectsOfType<FreezableStageObject>().Where(fso => fso.freeze).ToArray();
                foreach (FreezableStageObject fso in fsos)
                {
                    fso.freeze = false;
                }
            }

            if (state == PlayModeStateChange.ExitingPlayMode)
            {
                states = new Dictionary<GameObject, FreezeState>();
                FreezableStageObject[] fsos = GameObject.FindObjectsOfType<FreezableStageObject>().Where(fso => fso.freeze).ToArray();
                foreach (FreezableStageObject fso in fsos)
                {                    
                    states.Add(fso.gameObject, new FreezeState(fso.transform.position, fso.transform.rotation, fso.transform.localScale));
                }
            }

            if (state == PlayModeStateChange.EnteredEditMode)
            {                
                /*
                FreezableStageObject[] fsos = GameObject.FindObjectsOfType<FreezableStageObject>().Where(fso => fso.freeze).ToArray();
                foreach (FreezableStageObject fso in fsos)
                {
                    if(states != null && states.ContainsKey(fso.gameObject))
                    {
                        fso.transform.position = states[fso.gameObject].position;
                        fso.transform.rotation = states[fso.gameObject].rotation;
                    }                
                }
                */

                //This method is better to save object that have been tagged during the playmode and just ignoring the untagged one
                if (states != null && states.Count > 0)
                {
                    foreach (GameObject fsoGameObject in states.Keys)
                    {
                        FreezableStageObject fso = fsoGameObject.GetComponent<FreezableStageObject>();
                        fso.transform.position = states[fsoGameObject].position;
                        fso.transform.rotation = states[fsoGameObject].rotation;
                        fso.transform.localScale = states[fsoGameObject].scale;
                    }
                    states = null;
                }
            }
        }

    }
}