﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace StageTools
{
    [CustomEditor(typeof(FreezableStageObject))]
    [CanEditMultipleObjects]
    public class FreezableStageObjectEditor : Editor
    {
        static float size = .5f;
        static float pickSize = .5f;
        static Color frozenColor = Color.cyan;
        static Color unfrozenColor = new Color(1f, 0, 0, .5f);

        static Vector3 positionOffset = Vector3.up * 2f;

        static string saveIcon = "Packages/com.gnomik.stagetools/Gizmos/FreezeSave.png";
        static string ignoreIcon = "Packages/com.gnomik.stagetools/Gizmos/FreezeIgnore.png";

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }

        protected virtual void OnSceneGUI()
        {

            FreezableStageObject fso = (FreezableStageObject)target;

            Handles.color = fso.freeze ? frozenColor : unfrozenColor;
            //Handles.color = new Color(1f, 1f, 1f, 0);
            
            if (Handles.Button(fso.transform.position + positionOffset, Quaternion.identity, size, pickSize, Handles.SphereHandleCap))
            {
                fso.freeze = !fso.freeze;                
            }
            
            Handles.color = Color.white;
            //Handles.Label(fso.transform.position + positionOffset, fso.freeze ? "Unfreeze" : "Freeze", FreezableStageManager.guiStyle);
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.NotInSelectionHierarchy | GizmoType.Selected)]
        static void DrawGizmos(FreezableStageObject fso, GizmoType gizmoType)
        {
            Gizmos.color = fso.freeze ? frozenColor : unfrozenColor;

            //Gizmos.DrawSphere(fso.transform.position + positionOffset, size * .5f);

         
            Gizmos.DrawIcon(fso.transform.position + positionOffset, fso.freeze ? saveIcon : ignoreIcon, true, Gizmos.color);
        }
    }
}