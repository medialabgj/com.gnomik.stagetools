﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StageTools
{
    public class StageCameraSceneGUI : EditorWindow
    {
        private const string DISPLAY_GUI_MENU_NAME = "Stage tools/Camera/Display GUI";
        private const string CAPTURE_MENU_NAME = "Stage tools/Camera/Capture";
        private static bool _guiEnabled = false;

        private static StageCamera stageCamera;

        [MenuItem(DISPLAY_GUI_MENU_NAME)]
        public static void ToggleGUI()
        {
            _guiEnabled = !_guiEnabled;
            if (_guiEnabled)
            {
                stageCamera = GameObject.FindObjectOfType<StageCamera>();
                if (stageCamera != null)
                {
                    EnableGUI();
                }
                else
                {
                    _guiEnabled = false;
                }

            }
            else
            {
                SceneView.duringSceneGui -= OnDuringSceneGUI;
            }

        }

        [MenuItem(CAPTURE_MENU_NAME)]
        public static void Capture()
        {
            if (stageCamera == null)
            {
                stageCamera = GameObject.FindObjectOfType<StageCamera>();
            }

            if (stageCamera != null)
            {
                if (stageCamera.CapturePosition())
                {
                    foreach (SceneView scene in SceneView.sceneViews)
                    {
                        scene.ShowNotification(new GUIContent("Capture done"));
                    }
                }
            }
        }

        static void EnableGUI()
        {
            _guiEnabled = true;
            SceneView.duringSceneGui += OnDuringSceneGUI;
            Menu.SetChecked(DISPLAY_GUI_MENU_NAME, true);
        }

        static void DisableGUI()
        {
            _guiEnabled = false;
            SceneView.duringSceneGui -= OnDuringSceneGUI;
            Menu.SetChecked(DISPLAY_GUI_MENU_NAME, false);
        }

        private static void OnDuringSceneGUI(SceneView sceneview)
        {
            if (stageCamera == null)
            {
                DisableGUI();
                return;
            }
            else
            {
                Handles.BeginGUI();

                GUILayout.BeginArea(new Rect(10, 10, 96f, 248f));
                if (GUILayout.Button("Find camera"))
                {
                    Selection.activeGameObject = stageCamera.gameObject;
                    SceneView.FrameLastActiveSceneView();
                }

                if (GUILayout.Button("Reset position"))
                {
                    stageCamera.ResetPosition();
                }

                if (GUILayout.Button("Capture"))
                {
                    Capture();
                }
                GUILayout.EndArea();
                Handles.EndGUI();
            }



        }
    }
}