﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StageTools
{
    [CustomEditor(typeof(StageCamera))]
    public class StageCameraEditor : Editor
    {
        static Vector3 newLocalPosition;

        //static string zoomIcon = "Packages/com.gnomik.stagetools/Gizmos/IconZoom.png";
        protected virtual void OnSceneGUI()
        {
            StageCamera stageCamera = (target as StageCamera);

            float size = HandleUtility.GetHandleSize(stageCamera.transform.position) * .5f;
            Vector3 snap = Vector3.one * 2;

            /*
            Handles.color = stageCamera.localPosition.z <= 0 ? Color.cyan : Color.red;

            EditorGUI.BeginChangeCheck();

            Handles.matrix = stageCamera.transform.localToWorldMatrix;

            newLocalPosition = Handles.FreeMoveHandle(stageCamera.localPosition, Quaternion.identity, size * 2f, snap, Handles.CircleHandleCap);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(stageCamera, "Change stage camera local position");
                stageCamera.localPosition = Vector3.Scale(newLocalPosition, Vector3.forward);
                stageCamera.transform.localPosition = stageCamera.localPosition;
            }
            */


            /*
            Handles.matrix = Matrix4x4.identity;
            if (stageCamera.localPosition != Vector3.zero && Handles.Button(stageCamera.transform.position + Vector3.up * size, Quaternion.identity, size, size, Handles.SphereHandleCap))
            {
                stageCamera.ResetPosition();
                //Selection.activeGameObject = stageCamera.gameObject;
                //SceneView.FrameLastActiveSceneView();
            }
            */
            /*
            Handles.color = Color.green;
            if (Application.isPlaying && Handles.Button(stageCamera.transform.position, stageCamera.transform.rotation * Quaternion.Euler(0, 180f, 0), size, size, Handles.ConeHandleCap))
            {
                if (Application.isPlaying)
                {
                    stageCamera.CapturePosition();
                }
                else
                {
                    Debug.LogWarning("Capture cannot start because application is not running");
                }

            }
            */

        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Capture"))
            {
                (target as StageCamera).CapturePosition();
            }
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected)]
        static void DrawGizmos(StageCamera stageCamera, GizmoType gizmoType)
        {
            if (stageCamera.states != null && stageCamera.states.Count > 0)
            {
                foreach (StageCameraState state in stageCamera.states)
                {
                    Gizmos.color = Color.cyan;
                    Gizmos.DrawSphere(state.position, 1f);
                }

                if (stageCamera.states.Count > 1)
                {
                    for (int i = 0; i < stageCamera.states.Count - 1; i++)
                    {
                        Gizmos.color = Color.white;
                        Gizmos.DrawLine(stageCamera.states[i].position, stageCamera.states[i + 1].position);
                    }
                }
            }



            //Gizmos.DrawIcon(newLocalPosition, zoomIcon);
        }

    }
}