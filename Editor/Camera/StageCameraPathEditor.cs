using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using System.Linq;

namespace StageTools
{
    [CustomEditor(typeof(StageCameraPath))]
    public class StageCameraPathEditor : Editor
    {
        private StageCameraPath path;
        private Transform handleTransform;
        private Quaternion handleRotation;

        private Quaternion globalRotation = Quaternion.identity;
        
        private int selectedIndex = -1;
        private int selectedTangentIndex = -1;

        private GUIStyle labelStyle;

        static int flags = -1;
        static bool lockTarget = false;        
        static bool lockRealTime = false;        
        public float lockTime;
        public float pathTotalTime;
        WaypointPathState lockState;

        bool foldoutComputeTime = false;
        bool foldoutComputeSpeed = false;
        bool foldoutDebugTools = false;

        public string globalTotalTimeText;
        public string globalSpeedText;
        public int globalTotalTime;

        

        static string[] options = new string[] { "Waypoint", "Tangeants", "Split" };

        private const float handleSize = 0.12f;
        private const float pickSize = 0.20f;

        static Color[] handleColors = new Color[] { Color.white, Color.cyan, new Color(1f, 0, 1f) };
        static Color selectedColor = Color.yellow;

        private void OnEnable()                            
        {
            labelStyle = new GUIStyle();
            labelStyle.fontSize = 24;
            labelStyle.normal.textColor = Color.white;
        }

        public override void OnInspectorGUI()
        {
            flags = EditorGUILayout.MaskField("Point view", flags, options);            
            lockTarget = EditorGUILayout.Toggle("Lock Target", lockTarget);
            EditorGUI.indentLevel++;
            if(path != null && lockTarget)
            {
                lockRealTime = EditorGUILayout.Toggle("Realtime", lockRealTime);

                pathTotalTime = path.GetTotalTime();

                EditorGUILayout.BeginHorizontal();
                
                EditorGUILayout.LabelField("Time", GUILayout.Width(50));
                lockTime = EditorGUILayout.Slider(lockTime, 0, (lockRealTime ? pathTotalTime : 1f), GUILayout.ExpandWidth(true));
                

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Waypoint: " + lockState.index);
                if(!lockRealTime)
                {                                    
                    EditorGUILayout.LabelField("Waypoint time: " + lockState.time);
                }
                
                EditorGUILayout.LabelField("Total time: " + pathTotalTime);

                EditorGUILayout.EndHorizontal();

                DrawTarget();

                if (GUILayout.Button("Split at locked position"))
                {
                    (target as StageCameraPath).SplitAt(lockState.index, lockState.time);
                }
            }

            
            EditorGUI.indentLevel--;

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            if (path != null && path.playbackMode != StageCameraPathPlaybackMode.PositionalCurve)
            {

                foldoutComputeTime = EditorGUILayout.Foldout(foldoutComputeTime, "Compute time");

                if (foldoutComputeTime)
                {
                    EditorGUI.indentLevel++;


                    EditorGUILayout.LabelField("Total time");
                    globalTotalTimeText = GUILayout.TextField(globalTotalTimeText);
                    if (!string.IsNullOrEmpty(globalTotalTimeText))
                    {
                        globalTotalTimeText = Regex.Replace(globalTotalTimeText, @"[^0-9]", "");
                        int.TryParse(globalTotalTimeText, out globalTotalTime);
                    }
                    else
                    {
                        globalTotalTime = 0;
                    }


                    if (GUILayout.Button(new GUIContent("Distribute total time", "Distributes the above value as the total time of the path with respect to the length of each point")) && globalTotalTime > 0)
                    {
                        if (path != null)
                        {
                            path.DistributeTotalTime(globalTotalTime);
                        }
                    }


                    EditorGUI.indentLevel--;
                }


                foldoutComputeSpeed = EditorGUILayout.Foldout(foldoutComputeSpeed, "Compute speed");

                if (foldoutComputeSpeed)
                {
                    EditorGUI.indentLevel++;
                    if (GUILayout.Button(new GUIContent("Time from speed", "Compute the time for each waypoints with speed above zero in respect of their length")))
                    {
                        (target as StageCameraPath).ComputeTimeFromSpeed();
                    }

                    EditorGUILayout.LabelField("Global speed");
                    globalSpeedText = GUILayout.TextField(globalSpeedText);
                    if (!string.IsNullOrEmpty(globalTotalTimeText))
                    {
                        globalSpeedText = Regex.Replace(globalSpeedText, @"[^0-9]", "");
                        int.TryParse(globalSpeedText, out globalTotalTime);
                    }
                    else
                    {
                        globalTotalTime = 0;
                    }

                    if (GUILayout.Button(new GUIContent("Mass assign speed", "Assign the above value as the speed for all waypoints")))
                    {
                        if (path != null)
                        {
                            foreach (StageCameraWaypoint wp in path.waypoints)
                            {
                                wp.speed = globalTotalTime;
                                path.ComputeTimeFromSpeed();
                            }

                        }
                    }

                    EditorGUI.indentLevel--;
                }

                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            }

            if (Application.isPlaying)
            {
                EditorGUILayout.BeginHorizontal();

                if (GUILayout.Button("Start", GUILayout.Width(80)))
                {
                    lockTarget = false;
                    (target as StageCameraPath).LaunchSequence();
                }

                if (GUILayout.Button("Stop", GUILayout.Width(80)))
                {
                    lockTarget = false;
                    (target as StageCameraPath).StopSequence();
                }

                if (GUILayout.Button("Reset", GUILayout.Width(80)))
                {
                    lockTarget = false;
                    (target as StageCameraPath).ResetSequence();
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            }

            base.OnInspectorGUI();

            if(path.sampledPath.waypoints != null && path.sampledPath.waypoints.Length > 0)
            {
                float animationTime = 0;
                if (path.sampledPath.usePositionnalCurve)
                {
                    animationTime = path.positionalCurve.keys.Max(k => k.time);                    
                }
                else if(path.sampledPath.time > 0)
                {
                    animationTime = path.sampledPath.time;
                } 
                else if (path.sampledPath.totalDistance > 0 && path.sampledPath.speed > 0)
                {
                    animationTime = path.sampledPath.totalDistance / path.sampledPath.speed;
                } 
                

                if(animationTime > 0)
                {
                    GUILayout.Label($"Animation time will be {animationTime} seconds");
                } else
                {
                    GUILayout.Label("Error, speed wasn't set and/or waypoints weren't sampled");
                }                

            }


            EditorGUILayout.BeginHorizontal("box");

            if (GUILayout.Button("Sample path by distance"))
            {                

                List<SampledWaypoint> sampledWaypoints = new List<SampledWaypoint>();
                path.sampledPath.totalDistance = 0;

                WaypointPathState firstState = path.GetStateFromGlobalTime(0);

                sampledWaypoints.Add(new SampledWaypoint(firstState.position, firstState.rotation));

                for (int i = 1; i <= path.sampledResolution; i++)
                {
                    WaypointPathState state = path.GetStateFromGlobalTime(i / (float)path.sampledResolution);

                    if(Vector3.Distance(state.position, sampledWaypoints[sampledWaypoints.Count-1].position) > path.sampledDistance)
                    {
                        sampledWaypoints.Add(new SampledWaypoint(state.position, state.rotation));                        
                    }
                    
                }

                sampledWaypoints.Add(new SampledWaypoint(path.waypoints.Last().position, path.waypoints.Last().rotation));

                if(sampledWaypoints.Count > 1)
                {
                    path.sampledPath.waypoints = sampledWaypoints.ToArray();
                }

                path.sampledPath.ComputeTotalDistance();
            }

            

            if (GUILayout.Button("Sample path by resolution"))
            {
                path.sampledPath.waypoints = new SampledWaypoint[path.sampledResolution + 1];
                path.sampledPath.totalDistance = 0;
                for (int i = 0; i <= path.sampledResolution; i++)
                {
                    WaypointPathState state = path.GetStateFromGlobalTime(i / (float)path.sampledResolution);
                    path.sampledPath.waypoints[i] = new SampledWaypoint(state.position, state.rotation);
                }
                path.sampledPath.ComputeTotalDistance();
            }

            EditorGUILayout.EndHorizontal();


            foldoutDebugTools = EditorGUILayout.Foldout(foldoutDebugTools, "Debug tools");

            if (foldoutDebugTools)
            {
                EditorGUI.indentLevel++;
                if (GUILayout.Button("Reset tangents"))
                {
                    path.ResetTangents();
                }

                if (GUILayout.Button("Flatten"))
                {
                    foreach (StageCameraWaypoint w in path.waypoints)
                    {
                        w.position.y = 0;
                        w.tangents[0].y = 0;
                        w.tangents[1].y = 0;
                    }
                }

                EditorGUI.indentLevel--;
            }
        }

        public void FocusPosition(Vector3 position, float scale = 1f)
        {
            SceneView.lastActiveSceneView.Frame(new Bounds(position, Vector3.one * scale), false);
        }

        void DrawGUILine(int i_height = 1)
        {
            Rect rect = EditorGUILayout.GetControlRect(false, i_height);
            rect.height = i_height;
            EditorGUI.DrawRect(rect, new Color(0.5f, 0.5f, 0.5f, 1));
        }
       

        private void OnSceneGUI()
        {
            path = target as StageCameraPath;
            handleTransform = path.transform;
            

            if(path.waypoints != null && path.waypoints.Length > 1)
            {
                for (int w = 0; w < path.waypoints.Length; w++)
                {
                    DrawWaypoint(w);
                    
                    switch (Tools.current)
                    {
                        case Tool.Move:
                            ShowPointMove(w);
                            break;
                        case Tool.Rotate:
                            ShowPointRotate(w);
                            break;
                    }
                    
                }

                DrawPath(path);
                
            }
            
            
            if(Event.current.type == EventType.ExecuteCommand)
            {
                if(Event.current.commandName == "FrameSelected" && (lockTarget || selectedIndex != -1))
                {
                    Event.current.commandName = "";
                    Event.current.Use();

                    if(lockTarget)
                    {
                        FocusPosition(lockState.position);
                    } 
                    else if(selectedIndex != -1 && selectedTangentIndex == -1)
                    {
                        FocusPosition(handleTransform.TransformPoint(path.waypoints[selectedIndex].position));
                    }
                    else if (selectedIndex != -1 && selectedTangentIndex != -1)
                    {
                        FocusPosition(handleTransform.TransformPoint(path.waypoints[selectedIndex].tangents[selectedTangentIndex]));
                    }
                }
            }

            HandleKeyboard();
        }

        

        private  void HandleKeyboard()
        {            
            Event current = Event.current;
            if (current.type != EventType.KeyDown || !(current.modifiers.HasFlag(EventModifiers.Control)))
                return;
            
            switch(current.keyCode)
            {
                case KeyCode.PageDown:
                case KeyCode.LeftArrow:
                    MoveToDelta(-1);
                    break;
                case KeyCode.PageUp:
                case KeyCode.RightArrow:
                    MoveToDelta(1);
                    break;

                case KeyCode.Home:
                    GoToPoint(0);
                    break;
                case KeyCode.End:
                    GoToPoint(path.waypoints.Length - 1);
                    break;
            }

            
        }

        private void MoveToDelta(int delta)
        {
            int newIndex = selectedIndex;
            newIndex += delta;
            if (delta >= 0)
            {
                if (newIndex >= path.waypoints.Length)
                {
                    newIndex = 0;
                }
            }
            else
            {
                if (newIndex < 0)
                {
                    newIndex = path.waypoints.Length - 1;
                }
            }

            GoToPoint(newIndex);
        }

        private void GoToPoint(int index)
        {
            selectedIndex = index;
            selectedTangentIndex = -1;
            FocusPosition(path.waypoints[selectedIndex].position, 4f);
        }

        private static void DrawPath(StageCameraPath path)
        {
            if(path == null || path.waypoints == null || path.waypoints.Length == 0)
            {
                return;
            }

            if (path.sampledPath.waypoints != null && path.sampledPath.waypoints.Length > 1)
            {
                Handles.color = Color.cyan;
                for (int i = 0; i < path.sampledPath.waypoints.Length - 1; i++)
                {
                    Handles.DrawLine(path.sampledPath.waypoints[i].position, path.sampledPath.waypoints[i + 1].position);
                }
            }


            for (int w = 0; w < path.waypoints.Length - 1; w++)
            {
                StageCameraWaypoint waypoint = path.waypoints[w];
                Handles.color = Color.white;
                Vector3 lineStart = path.GetPoint(w, 0f);
                //Handles.color = Color.green;
                //Handles.DrawLine(lineStart, lineStart + path.GetDirection(w, 0f));
                
                for (int i = 1; i <= path.lineSteps; i++)
                {
                    float t = i / (float)path.lineSteps;
                    Vector3 lineEnd = path.GetPoint(w, t);
                    Handles.color = Color.white;
                    Handles.DrawLine(lineStart, lineEnd);
                    //Handles.color = Color.green;
                    //Handles.DrawLine(lineEnd, lineEnd + path.GetDirection(w, i / (float)lineSteps));
                    Handles.color = new Color(1f, 1f, 0);
                    Handles.DrawLine(lineStart, lineStart + path.GetRotation(w, t) * Vector3.forward);
                    lineStart = lineEnd;
                }


                if (w < path.waypoints.Length - 1)
                {
                    Handles.color = Color.blue;
                    StageCameraWaypoint nextWaypoint = path.waypoints[w + 1];
                    if (waypoint.pathMode == WaypointPathMode.Bezier)
                    {
                        Handles.DrawBezier(path.transform.TransformPoint(waypoint.position), path.transform.TransformPoint(nextWaypoint.position), path.transform.TransformPoint(waypoint.tg2), path.transform.TransformPoint(nextWaypoint.tg1), Color.blue, null, 2f);
                    }
                    else
                    {
                        #if UNITY_2020
                        Handles.DrawLine(path.transform.TransformPoint(waypoint.position), path.transform.TransformPoint(nextWaypoint.position), 2f);
                        #else
                        Handles.DrawLine(path.transform.TransformPoint(waypoint.position), path.transform.TransformPoint(nextWaypoint.position));
                        #endif
                    }

                }
            }
        }


        private void SetWaypointAsTarget(int index)
        {
            if(lockTarget && !lockRealTime)
            {
                lockTime = index / (float)(path.waypoints.Length - 1);                
            }            
        }

        private void DrawTarget()
        {
            if (lockTarget)
            {                

                if(lockRealTime)
                {
                    if(path.playbackMode == StageCameraPathPlaybackMode.PositionalCurve)
                    {
                        lockState = path.GetStateFromGlobalTime(path.positionalCurve.Evaluate(lockTime));
                    } else
                    {
                        lockState = path.GetStateFromGlobalRealTime(lockTime);
                    }
                    
                } 
                else
                {
                    lockState = path.GetStateFromGlobalTime(lockTime);                    
                }
                                
                path.UpdateTargetFromState(lockState);                                    
          
            }
        }

        private void DrawWaypoint(int index)
        {
            StageCameraWaypoint waypoint = path.waypoints[index];         
            if(selectedIndex == index)
            {
                labelStyle.normal.textColor = selectedColor;
            } else
            {
                labelStyle.normal.textColor = Color.gray;
            }
            Handles.Label(handleTransform.TransformPoint(waypoint.position) + Vector3.up * 1.5f, index.ToString(), labelStyle);

            if ((flags & 1) == 1)
            {
                ShowPointSelector(index);
            }

            if((flags & 4) == 4)
            {
                ShowSplitButton(index);
            }
            
        }

        private Vector3 ShowPointSelector(int index)
        {
            StageCameraWaypoint waypoint = path.waypoints[index];
            Vector3 point = handleTransform.TransformPoint(waypoint.position);

            float size = HandleUtility.GetHandleSize(point);
            Handles.color = selectedIndex == index && selectedTangentIndex == -1 ? selectedColor : handleColors[(int)waypoint.pointMode];

            if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
            {
                selectedIndex = index;
                selectedTangentIndex = -1;
                SetWaypointAsTarget(index);
                Repaint();
            }
            return point;
        }

        private void ShowSplitButton(int index)
        {
            if(index >= path.waypoints.Length -1)
            {
                return;
            }

            if(selectedIndex == index && selectedTangentIndex == -1)
            {
                if(Handles.Button(path.GetPoint(index, .5f), Quaternion.identity, 1f, 1f, Handles.SphereHandleCap))
                {
                    if(EditorUtility.DisplayDialog("Confirm split", "Are you sure to split the path at this point ?", "Confirm", "Cancel"))
                    {
                        path.SplitAt(index);
                    }                    
                }
            }
        }

        private void ShowControlPoint(int index)
        {
            StageCameraWaypoint waypoint = path.waypoints[index];            
            
            for(int c = 0; c <= 1; c++)
            {
                Vector3 cp = handleTransform.TransformPoint(waypoint.tangents[c]);

                float size = HandleUtility.GetHandleSize(cp) * .7f;

                Color baseColor = selectedIndex == index && selectedTangentIndex == c ? selectedColor : handleColors[(int)waypoint.pointMode];

                Handles.color = baseColor * .7f;
                #if UNITY_2020
                Handles.DrawLine(handleTransform.TransformPoint(waypoint.position), handleTransform.TransformPoint(waypoint.tangents[c]), 2f);
                #else
                Handles.DrawLine(handleTransform.TransformPoint(waypoint.position), handleTransform.TransformPoint(waypoint.tangents[c]));
                #endif
                Handles.color = baseColor * .8f;

                if (Handles.Button(cp, handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
                {
                    selectedIndex = index;
                    selectedTangentIndex = c;
                    Repaint();
                }

                if (selectedIndex == index && selectedTangentIndex == c)
                {
                    EditorGUI.BeginChangeCheck();
                    handleRotation = Quaternion.identity;
                    cp = Handles.DoPositionHandle(cp, handleRotation);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(path, "Move Control Point");
                        EditorUtility.SetDirty(path);
                        waypoint.UpdateControlPoint(c, handleTransform.InverseTransformPoint(cp));
                    }
                    
                }
            }




        }

        private Vector3 ShowPointMove(int index)
        {
            StageCameraWaypoint waypoint = path.waypoints[index];
            Vector3 point = handleTransform.TransformPoint(waypoint.position);

            if (selectedIndex == index && selectedTangentIndex == -1 && (flags & 1) == 1)
            {
                EditorGUI.BeginChangeCheck();
                handleRotation = Tools.pivotRotation == PivotRotation.Local ?
                                waypoint.rotation : Quaternion.identity;
                point = Handles.DoPositionHandle(point, handleRotation);
                if (EditorGUI.EndChangeCheck())
                {
                    
                    Undo.RecordObject(path, "Move Point");
                    EditorUtility.SetDirty(path);
                    waypoint.UpdatePosition(handleTransform.InverseTransformPoint(point));                    
                }
            }

            if ((flags & 2) == 2)
            {
                ShowControlPoint(index);
            }

            return point;
        }

        private Quaternion ShowPointRotate(int index)
        {
            StageCameraWaypoint waypoint = path.waypoints[index];
            Quaternion endRotation = waypoint.rotation;                        
          
            if (selectedIndex == index)
            {
                //Quaternion startRotation = Tools.pivotRotation == PivotRotation.Local ? waypoint.rotation : Quaternion.identity;                
                Quaternion startRotation = waypoint.rotation;
                
                EditorGUI.BeginChangeCheck();                
                endRotation = Handles.RotationHandle(startRotation, handleTransform.TransformPoint(waypoint.position));

                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(path, "Rotate Point");
                    
                    EditorUtility.SetDirty(path);
                    waypoint.rotation = endRotation;
                    /*
                    if (Tools.pivotRotation == PivotRotation.Local)
                    {                        
                        waypoint.rotation = endRotation;
                    } else
                    {                        
                        
                        if(Event.current.type == EventType.MouseUp)
                        {
                            Quaternion d = endRotation * Quaternion.Inverse(startRotation);
                            float angle;
                            Vector3 axis;
                            d.ToAngleAxis(out angle, out axis);
                            waypoint.rotation = Quaternion.AngleAxis(1f, axis) * waypoint.rotation;
                        }                        
                        
                    }
                    */
                }
            }
            return endRotation;
        }

        [DrawGizmo(GizmoType.NonSelected)]
        static void RenderGizmoNotSelected(StageCameraPath path, GizmoType gizmoType)
        {
            DrawPath(path);
        }
    }
}