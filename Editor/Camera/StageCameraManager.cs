﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace StageTools
{
    [InitializeOnLoadAttribute]
    public class StageCameraManager
    {
        static Dictionary<string, StageCameraState[]> cameraStates;
        static int sessionIndex;
        static Color[] pointColors = { Color.red, Color.blue, Color.green, new Color(1f, 1f, 0), new Color(1f, .5f, 0), new Color(.5f, 0, 1f) };
        static StageCameraManager()
        {
            EditorApplication.playModeStateChanged += HandlePlayModeChange;
        }

        private static void HandlePlayModeChange(PlayModeStateChange state)
        {

            switch (state)
            {
                case PlayModeStateChange.ExitingPlayMode:
                    SaveCameraPositions();
                    break;
                case PlayModeStateChange.EnteredEditMode:
                    GenerateObjects();
                    break;
            }

        }

        static void SaveCameraPositions()
        {
            cameraStates = new Dictionary<string, StageCameraState[]>();

            StageCamera[] stageCameras = GameObject.FindObjectsOfType<StageCamera>();
            foreach (StageCamera stageCamera in stageCameras)
            {
                if (stageCamera.states == null || stageCamera.states.Count == 0)
                {
                    continue;
                }

                cameraStates.Add(stageCamera.gameObject.name, stageCamera.states.ToArray());
            }
        }

        static void GenerateObjects()
        {
            if (cameraStates != null && cameraStates.Count > 0)
            {
                sessionIndex = GameObject.FindObjectsOfType<StageCameraStorage>().Length;
                foreach (KeyValuePair<string, StageCameraState[]> cameraState in cameraStates)
                {
                    GameObject container = new GameObject(cameraState.Key + "-Storage-" + sessionIndex);
                    container.AddComponent<StageCameraStorage>();
                    container.GetComponent<StageCameraStorage>().pointColor = pointColors[sessionIndex % pointColors.Length];
                    int index = 0;
                    foreach (StageCameraState state in cameraState.Value)
                    {
                        GameObject child = new GameObject("state-" + index);
                        child.transform.parent = container.transform;
                        child.transform.position = state.position;
                        child.transform.rotation = state.rotation;
                        index++;
                    }
                }
            }
        }
    }
}