﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StageTools
{
    [CustomEditor(typeof(StageCameraStorage))]
    public class StageCameraStorageEditor : Editor
    {
        static string pointIcon = "Packages/com.gnomik.stagetools/Gizmos/IconPoint.png";

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Generate waypoints"))
            {
                GenerateCameraWaypoints();
            }
        }

        void GenerateCameraWaypoints()
        {
            StageCameraStorage storage = target as StageCameraStorage;
            if (storage.transform.childCount == 0)
            {
                Debug.LogWarning("No points were found");
                return;
            }


            Transform waypointContainer = new GameObject("Waypoints").transform;
            StageCameraPath path = waypointContainer.gameObject.AddComponent<StageCameraPath>();
            path.waypoints = new StageCameraWaypoint[storage.transform.childCount];

            for (int i = 0; i < storage.transform.childCount; i++)
            {
                StageCameraWaypoint waypoint = new StageCameraWaypoint();
                waypoint.position = storage.transform.GetChild(i).position;
                waypoint.rotation = storage.transform.GetChild(i).rotation;

                path.waypoints[i] = waypoint;
            }

            path.ResetTangents();
        }

        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected)]
        static void DrawGizmos(StageCameraStorage stageCameraStorage, GizmoType gizmoType)
        {
            foreach (Transform child in stageCameraStorage.transform)
            {
                Gizmos.color = stageCameraStorage.pointColor;
                Gizmos.DrawIcon(child.transform.position, pointIcon, false, Gizmos.color);
                //Gizmos.DrawRay(child.transform.position, child.transform.rotation * Vector3.forward);
                Gizmos.color = Color.white;
                Gizmos.DrawLine(child.transform.position, child.transform.position + (child.transform.rotation * Vector3.forward) * 3f);

            }

            if (stageCameraStorage.transform.childCount > 1)
            {
                for (int i = 0; i < stageCameraStorage.transform.childCount - 1; i++)
                {
                    Gizmos.color = Color.white;
                    Gizmos.color = stageCameraStorage.pointColor;                    
                    Gizmos.DrawLine(stageCameraStorage.transform.GetChild(i).position, stageCameraStorage.transform.GetChild(i + 1).position);
                }
            }
        }
    }
}