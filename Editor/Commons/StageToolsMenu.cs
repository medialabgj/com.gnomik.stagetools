﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace StageTools
{
    public static class StageToolsMenu
    {
        /*
        static StageToolsMenu()
        {
            Menu.SetChecked("Stage tools/Freezer/Unfreeze on play", FreezableStageManager.unfreezeOnPlay);
        }
        */

        [MenuItem("Stage tools/Freezer/Freeze all")]
        static void FreezeAll()
        {
            var freezables = GameObject.FindObjectsOfType<FreezableStageObject>();
            foreach(FreezableStageObject freezable in freezables)
            {
                freezable.freeze = true;
            }
        }

        [MenuItem("Stage tools/Freezer/Unfreeze all")]
        static void UnfreezeAll()
        {
            var freezables = GameObject.FindObjectsOfType<FreezableStageObject>();
            foreach (FreezableStageObject freezable in freezables)
            {
                freezable.freeze = false;
            }
        }

        [MenuItem("Stage tools/Freezer/Unfreeze on play")]
        static void UnfreezeOnPlay()
        {
            FreezableStageManager.unfreezeOnPlay = !FreezableStageManager.unfreezeOnPlay;
            Menu.SetChecked("Stage tools/Freezer/Unfreeze on play", FreezableStageManager.unfreezeOnPlay);            
        }
    }

}