using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StageTools
{


    public enum WaypointPathMode { Bezier, Linear };
    public enum WaypointPointMode { Mirrored, Aligned, Free };
    public enum WaypointRotationMode { Waypoint, FollowPath, BlendLookAt, HardLookAt, WaypointBlend };

    [System.Serializable]
    public class StageCameraWaypoint
    {
        public WaypointPointMode pointMode;
        public WaypointPathMode pathMode;
        public WaypointRotationMode rotationMode;

        public Vector3 position;
        public Quaternion rotation;

        [HideInInspector]
        public float length;

        [HideInInspector]
        public float ratio;

        public float time = 1f;
        [SerializeField]
        private float m_speed;
        
        public float speed
        {
            get
            {
                return m_speed;
            }

            set
            {                
                m_speed = value;                
            }
        }
        public float delay = 0;
        public AnimationCurve movementCurve;
        public AnimationCurve rotationCurve;
        public AnimationCurve blendRotationCurve;

        public Transform lookAtTarget;        

        [HideInInspector]
        public Vector3[] tangents = new Vector3[2];

        public Vector3 tg1
        {
            get
            {
                return tangents[0];
            }

            set
            {
                tangents[0] = value;
            }
        }
        
        public Vector3 tg2
        {
            get
            {
                return tangents[1];
            }

            set
            {
                tangents[1] = value;
            }
        }

        public StageCameraWaypoint()
        {            
            movementCurve = AnimationCurve.Linear(0, 0, 1f, 1f);
            rotationCurve = AnimationCurve.Linear(0, 0, 1f, 1f);
            blendRotationCurve = AnimationCurve.Linear(0, 0, 1f, 1f);            
        }        
         
        public void UpdateControlPoint(int index, Vector3 value)
        {
            int otherIndex = index == 0 ? 1 : 0;
            tangents[index] = value;
            
            if(pointMode == WaypointPointMode.Free)
            {
                return;
            }

            Vector3 middle = position;
            Vector3 enforcedTangent = middle - tangents[index];            
            if(pointMode == WaypointPointMode.Aligned)
            {
                enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, tangents[otherIndex]);
            }
            tangents[otherIndex] = middle + enforcedTangent;
        }

        public void UpdatePosition(Vector3 value)
        {
            Vector3 delta = value - position;
            position = value;

            tangents[0] += delta;
            tangents[1] += delta;
        }

        public void ResetTangents()
        {
            tg1 = position + position.normalized * -1f;
            tg2 = position + position.normalized * 1f;
        }
    }

}