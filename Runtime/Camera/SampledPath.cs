using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StageTools
{
     [System.Serializable]
    public struct SampledPath
    {
        public float time;
        public float speed;        
        public float totalDistance;
        public bool usePositionnalCurve;
        public bool clampCurveValue;
        public SampledWaypoint[] waypoints;

        public float fullAnimationTime
        {
            get
            {
                if (time > 0)
                {
                    return time;
                }
                if (speed > 0)
                {
                    return totalDistance / speed;
                }
                return Mathf.Infinity;
            }
        }

        public void ComputeTotalDistance()
        {
            totalDistance = 0;
            for (int i = 0; i < waypoints.Length - 1; i++)
            {
                waypoints[i].distanceToNextPoint = Vector3.Distance(waypoints[i].position, waypoints[i + 1].position);
                totalDistance += waypoints[i].distanceToNextPoint;
            }

            float normalizedTotalTime = 0;

            for (int i = 0; i < waypoints.Length; i++)
            {
                waypoints[i].normalizedTimeToNextPoint = waypoints[i].distanceToNextPoint / totalDistance;
                waypoints[i].normalizedTimeFromPoint = normalizedTotalTime;
                normalizedTotalTime += waypoints[i].normalizedTimeToNextPoint;
                //Debug.Log($"Time from: {waypoints[i].normalizedTimeFromPoint}; Time to: {waypoints[i].normalizedTimeToNextPoint}");
            }
        }
    }

    [System.Serializable]
    public struct SampledWaypoint
    {
        public Vector3 position;
        public Quaternion rotation;
        public float distanceToNextPoint;
        public float normalizedTimeFromPoint;
        public float normalizedTimeToNextPoint;

        public SampledWaypoint(Vector3 p, Quaternion r)
        {
            position = p;
            rotation = r;
            distanceToNextPoint = 0;
            normalizedTimeFromPoint = 0;
            normalizedTimeToNextPoint = 0;
        }
    }

}