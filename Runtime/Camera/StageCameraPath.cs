using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace StageTools
{
    public struct WaypointPathState
    {
        public Vector3 position;
        public Quaternion rotation;

        public int index;
        public float time;

        public WaypointPathState(Vector3 p, Quaternion r, int i, float t)
        {
            position = p;
            rotation = r;
            index = i;
            time = t;
        }
    }

    public enum StageCameraPathPlaybackMode { Iterative, GlobalTime, PositionalCurve, SampledCurve };

    public class StageCameraPath : MonoBehaviour
    {
        public Transform target;

        public int lineSteps = 10;        

        public StageCameraPathPlaybackMode playbackMode;

        public AnimationCurve positionalCurve;

        public StageCameraWaypoint[] waypoints;

        public int sampledResolution = 100;
        public float sampledDistance = 10;

        public SampledPath sampledPath;

        [HideInInspector]
        public float timeCursor;

        private void Awake()
        {
            ComputeRatios();
        }

        public static void ResetAllSequence()
        {
            foreach(var stageCameraPath in GameObject.FindObjectsOfType<StageCameraPath>())
            {
                stageCameraPath.ResetSequence();
            }
        }

        public void ResetSequence()
        {
            if(target == null)
            {
                Debug.LogWarning($"There is no target object assigned to the path {gameObject.name}");
                return;
            }

            StopAllCoroutines();

            target.transform.position = GetPoint(0, 0);
            target.transform.rotation = GetRotation(0, 0);
        }

        public void StopSequence()
        {
            StopAllCoroutines();
        }

        public void LaunchSequence()
        {
            if(target == null)
            {
                Debug.LogWarning($"There is no target object assigned to the path {gameObject.name}");
                return;
            }
            
            StopAllCoroutines();

            switch(playbackMode)
            {
                case StageCameraPathPlaybackMode.GlobalTime:
                    StartCoroutine(GlobalTimeSequence());
                    break;
                case StageCameraPathPlaybackMode.Iterative:
                    StartCoroutine(IterativeSequence());
                    break;
                case StageCameraPathPlaybackMode.PositionalCurve:
                    StartCoroutine(PositionalCurveSequence());
                    break;
                case StageCameraPathPlaybackMode.SampledCurve:
                    if(sampledPath.usePositionnalCurve)
                    {
                        StartCoroutine(SampledCurveByPositionnalCurve());
                    } else
                    {
                        StartCoroutine(SampledCurveSequenceByTime());
                    }
                    
                    break;
            }
                        
        }

        IEnumerator IterativeSequence()
        {
            if (waypoints == null || waypoints.Length < 1)
            {
                Debug.LogWarning("There is not enough waypoints");
                yield break;
            }

            for (int i = 0; i < waypoints.Length - 1; i++)
            {
                StageCameraWaypoint waypoint = waypoints[i];

                target.transform.position = GetPoint(i, 0);
                target.transform.rotation = GetRotation(i, 0);

                yield return new WaitForSeconds(waypoint.delay);

                float animationTime = waypoint.time;
                float elapsedTime = 0;

                while (elapsedTime < animationTime)
                {
                    elapsedTime += Time.deltaTime;
                    float t = elapsedTime / animationTime;

                    if (target != null)
                    {                        
                        target.transform.position = GetPoint(i, t);
                        target.transform.rotation = GetRotation(i, t);
                    }
                    
                    yield return new WaitForEndOfFrame();
                }
            }
          
        }
        
        IEnumerator GlobalTimeSequence()
        {
            if (waypoints == null || waypoints.Length < 1)
            {
                Debug.LogWarning("There is not enough waypoints");
                yield break;
            }
            
            float animationTime = GetTotalTime();
            float elapsedTime = 0;
            
            while (elapsedTime < animationTime)
            {
                elapsedTime += Time.deltaTime;
                elapsedTime = Mathf.Clamp(elapsedTime, 0, animationTime);

                WaypointPathState lockState = GetStateFromGlobalRealTime(elapsedTime);

                UpdateTargetFromState(lockState);
                
                yield return new WaitForEndOfFrame();
            }
        }

        IEnumerator PositionalCurveSequence()
        {
            if (waypoints == null || waypoints.Length < 1)
            {
                Debug.LogWarning("There is not enough waypoints");
                yield break;
            }
                        
            float animationTime = GetTotalTime();
            float elapsedTime = 0;

            ComputeRatios();

            while (elapsedTime < animationTime)
            {
                elapsedTime += Time.deltaTime;
                elapsedTime = Mathf.Clamp(elapsedTime, 0, animationTime);
                
                WaypointPathState lockState = GetStateFromGlobalTime(positionalCurve.Evaluate(elapsedTime), false);
                UpdateTargetFromState(lockState);
                yield return new WaitForEndOfFrame();
            }
        }

        IEnumerator SampledCurveSequenceBySpeed()
        {
            float distanceTraveled = 0;
            float timePassed = 0;            

            for (int i = 0; i < sampledPath.waypoints.Length - 1; i++)
            {
                SampledWaypoint startWaypoint = sampledPath.waypoints[i];
                if( i > 0)
                {
                    startWaypoint.position = target.transform.position;
                    startWaypoint.rotation = target.transform.rotation;
                }

                SampledWaypoint endWaypoint = sampledPath.waypoints[i+1];

                Vector3 position = startWaypoint.position;
                Quaternion rotation = startWaypoint.rotation;                
                
                float speed = sampledPath.speed;                

                float startDistance = Vector3.Distance(position, endWaypoint.position);
                float distance = startDistance;

                while (distance > speed * Time.fixedDeltaTime)
                {
                    distance = Vector3.Distance(position, endWaypoint.position);
                    Vector3 direction = (endWaypoint.position - startWaypoint.position).normalized;
                    position += direction * speed * Time.fixedDeltaTime;
                    
                    float reverseT = Mathf.InverseLerp(startDistance, 0, distance);
                    rotation = Quaternion.Slerp(startWaypoint.rotation, endWaypoint.rotation, reverseT);
                    target.transform.position = position;
                    target.transform.rotation = rotation;
                    distanceTraveled += speed * Time.fixedDeltaTime;
                    timePassed += Time.fixedDeltaTime;                                      

                    yield return new WaitForFixedUpdate();
                }
                
                
            }
            
                
            yield return null; 
        }

        IEnumerator SampledCurveSequenceByTime()
        {

            Vector3 position;
            Quaternion rotation;
            float leftOver = 0;
            float totalTimePassed = 0;

            for (int i = 0; i < sampledPath.waypoints.Length - 1; i++)
            {
                SampledWaypoint startWaypoint = sampledPath.waypoints[i];
                if (i > 0)
                {
                    startWaypoint.position = target.transform.position;
                    startWaypoint.rotation = target.transform.rotation;
                }

                SampledWaypoint endWaypoint = sampledPath.waypoints[i + 1];

                float curveTime = (totalTimePassed) / sampledPath.fullAnimationTime;

                float speed = sampledPath.speed;
                float distance = Vector3.Distance(startWaypoint.position, endWaypoint.position);

                float animationTime = distance / speed;

                if (sampledPath.time > 0)
                {
                    animationTime = (distance / sampledPath.totalDistance) * sampledPath.time;
                }

                float elapsedTime = 0;

                while (elapsedTime < animationTime)
                {
                    elapsedTime += Time.deltaTime;
                    float t = elapsedTime / animationTime;                    
                    position = Vector3.Lerp(startWaypoint.position, endWaypoint.position, t);
                    rotation = Quaternion.Slerp(startWaypoint.rotation, endWaypoint.rotation, t);
                    target.transform.position = position;
                    target.transform.rotation = rotation;
                    yield return new WaitForEndOfFrame();
                }

                totalTimePassed += elapsedTime;
                leftOver = elapsedTime * sampledPath.speed;

            }

            yield return null;
        }

        IEnumerator SampledCurveByPositionnalCurve()
        {

            int currentPointIndex = 0;
            Vector3 position = sampledPath.waypoints[0].position;
            Quaternion rotation = sampledPath.waypoints[0].rotation;
            float animationTime = positionalCurve.keys.Max(k => k.time);
            float elapsedTime = 0;
            
            while (elapsedTime < animationTime)
            {
                elapsedTime += Time.deltaTime;
                float t = positionalCurve.Evaluate(elapsedTime);
                if (sampledPath.clampCurveValue)
                {
                    t = Mathf.Clamp01(t);
                }
                
                for(int i = 0; i < sampledPath.waypoints.Length; i++)
                {
                    if (t < sampledPath.waypoints[i].normalizedTimeFromPoint + sampledPath.waypoints[i].normalizedTimeToNextPoint)
                    {
                        currentPointIndex = i;
                        break;
                    }
                }

                
                if (currentPointIndex < sampledPath.waypoints.Length - 1)
                {
                    SampledWaypoint startWaypoint = sampledPath.waypoints[currentPointIndex];
                    SampledWaypoint endWaypoint = sampledPath.waypoints[currentPointIndex + 1];

                    float localT = (t - sampledPath.waypoints[currentPointIndex].normalizedTimeFromPoint) / sampledPath.waypoints[currentPointIndex].normalizedTimeToNextPoint;                                        
                    position = Vector3.Lerp(startWaypoint.position, endWaypoint.position, localT);
                    rotation = Quaternion.Slerp(startWaypoint.rotation, endWaypoint.rotation, localT);
                    target.transform.position = position;
                    target.transform.rotation = rotation;
                }
                yield return new WaitForEndOfFrame();
            }
            
            


            yield return null;
        }

        public Vector3 GetPoint(int index, float t)
        {
            index = Mathf.Clamp(index, 0, waypoints.Length - 1);

            if(index == waypoints.Length - 1)
            {
                return transform.TransformPoint(waypoints[index].position);
            }

            StageCameraWaypoint w = waypoints[index];
            StageCameraWaypoint wNext = waypoints[index + 1];

            t = waypoints[index].movementCurve.Evaluate(t);

            switch(waypoints[index].pathMode)
            {
                case WaypointPathMode.Bezier:                    
                    return transform.TransformPoint(Bezier.GetCubicPoint(w.position, w.tangents[1], wNext.tangents[0], wNext.position, t));
                default:
                    return transform.TransformPoint(Vector3.Lerp(w.position, wNext.position,t));                    
            }            
        }

        public Vector3 GetVelocity(int index, float t)
        {
            StageCameraWaypoint w = waypoints[index];
            StageCameraWaypoint wNext = waypoints[index + 1];
            return transform.TransformPoint(Bezier.GetCubicFirstDerivative(w.position, w.tangents[1], wNext.tangents[0],wNext.position, t)) - transform.position;            
        }

        public Vector3 GetDirection(int index, float t)
        {
            return GetVelocity(index, t).normalized;
        }

        public Quaternion GetRotation(int index, float t)
        {
            if (index == waypoints.Length - 1)
            {
                return waypoints[index].rotation;
            }

            float finalT = waypoints[index].rotationCurve.Evaluate(t);

            switch (waypoints[index].rotationMode)
            {
                case WaypointRotationMode.FollowPath:
                    return Quaternion.LookRotation(GetDirection(index, t));
                case WaypointRotationMode.BlendLookAt:
                    if (waypoints[index].lookAtTarget == null)
                    {
                        Debug.LogWarning("No look at target assigned");
                        return Quaternion.identity;
                    }
                    return Quaternion.Slerp(Quaternion.Slerp(waypoints[index].rotation, waypoints[index + 1].rotation, t), Quaternion.LookRotation((waypoints[index].lookAtTarget.position - GetPoint(index, t)).normalized), waypoints[index].blendRotationCurve.Evaluate(Mathf.PingPong(t * 2f, 1f)));
                case WaypointRotationMode.HardLookAt:
                    if(waypoints[index].lookAtTarget == null)
                    {
                        Debug.LogWarning("No look at target assigned");
                        return Quaternion.identity;
                    }                    
                    return Quaternion.LookRotation((waypoints[index].lookAtTarget.position - GetPoint(index, t)).normalized);
                case WaypointRotationMode.WaypointBlend:
                    return Quaternion.Slerp(waypoints[index].rotation, waypoints[index + 1].rotation, waypoints[index].rotationCurve.Evaluate(t));                    
                default:
                    return Quaternion.Slerp(waypoints[index].rotation, waypoints[index + 1].rotation, waypoints[index].rotationCurve.Evaluate(t));
            }
            
            
        }
       
        public WaypointPathState GetStateFromGlobalRealTime(float realTime)
        {                        
            float timeLeft = realTime;
            int lastIndex = 0;

            for(int i = 0; i < waypoints.Length; i++)
            {
                StageCameraWaypoint w = waypoints[i];
                timeLeft -= w.delay;
                if(timeLeft < 0)
                {
                    return new WaypointPathState(GetPoint(i, 0), GetRotation(i, 0), i, 0);
                }

                timeLeft -= w.time;
                
                if(timeLeft < 0)
                {
                    float t = Mathf.InverseLerp(0, w.time, w.time - Mathf.Abs(timeLeft));
                    return new WaypointPathState(GetPoint(i, t), GetRotation(i, t), i, t);
                }

                lastIndex = i;
            }

            return new WaypointPathState(GetPoint(lastIndex, 0), GetRotation(lastIndex, 0), lastIndex, 0);
        }

        public WaypointPathState GetStateFromGlobalTime(float time, bool computeRatios = true)
        {                                    
            if(computeRatios)
            {
                ComputeRatios();
            }
            
            float timeLeft = time;
            int lastIndex = 0;

            for (int i = 0; i < waypoints.Length; i ++)
            {
                timeLeft -= waypoints[i].ratio;
                if(timeLeft < 0)
                {
                    float t = Mathf.InverseLerp(0, waypoints[i].ratio, waypoints[i].ratio - Mathf.Abs(timeLeft));
                    return new WaypointPathState(GetPoint(i, t), GetRotation(i, t), i, t);
                }
                lastIndex = i;
            }

            return new WaypointPathState(GetPoint(lastIndex, 0), GetRotation(lastIndex, 0), lastIndex, 0);
        }

        public float GetTotalTime()
        {
            float totalTime = 0;

            if(playbackMode == StageCameraPathPlaybackMode.PositionalCurve)
            {
                return positionalCurve.keys.Max(k => k.time);
            }

            foreach(StageCameraWaypoint w in waypoints)
            {
                totalTime += w.delay;
                totalTime += w.time;
            }
            return totalTime;
        }  

        public void ComputeRatios()
        {
            float distance = GetTotalDistance();
            for (int i = 0; i < waypoints.Length - 1; i++)
            {
                waypoints[i].ratio = waypoints[i].length / distance;
            }
        }

        public float GetTotalDistance()
        {
            float distance = 0;
            for(int i = 0; i < waypoints.Length - 1; i ++)
            {
                distance += GetDistance(i);                
            }            
            return distance;
        }

        public float GetDistance(int index, int steps = 10)
        {
            steps = Mathf.Max(2, lineSteps);
            StageCameraWaypoint w = waypoints[index];
            float distance = 0;

            for (int i = 0; i < steps; i++)
            {                
                float t = i / (float)steps;
                float tNext = (i + 1) / (float)steps;
                distance += Vector3.Distance(GetPoint(index, t), GetPoint(index, tNext));                    
            }

            waypoints[index].length = distance;
            
            return distance;
        }

        private void OnValidate()
        {
            ComputeTimeFromSpeed();
        }

        public void DistributeTotalTime(float totalTime)
        {
            float totalDistance = GetTotalDistance();
            
            for(int i = 0; i < waypoints.Length - 1; i ++)
            {
                var w = waypoints[i];
                float distance = GetDistance(i);
                float t = distance / totalDistance;
                w.speed = 0;
                w.time = (float)totalTime * t;                
            }            
        }

        public void ComputeTimeFromSpeed()
        {
            if (waypoints == null || waypoints.Length < 2)
            {
                return;
            }

            for (int i = 0; i < waypoints.Length; i++)
            {
                StageCameraWaypoint waypoint = waypoints[i];
                if (i < waypoints.Length - 1 && waypoint.speed > 0)
                {
                    float distance = GetDistance(i);
                    waypoint.time = distance / waypoint.speed;
                }
            }
        }

        public void ResetTangents()
        {
            if(waypoints == null || waypoints.Length < 2)
            {
                return;
            }

            for (int i = 0; i < waypoints.Length; i++)
            {
                StageCameraWaypoint w = waypoints[i];

                w.pointMode = WaypointPointMode.Mirrored;

                if(i < waypoints.Length - 1)
                {
                    w.UpdateControlPoint(1, w.position + (waypoints[i+1].position - w.position).normalized);
                } else
                {
                    w.ResetTangents();
                }
                                
             
            }
        }

        public void UpdateTargetFromState(WaypointPathState lockState)
        {
            if(target == null)
            {
                Debug.LogWarning("No target selected");
                return;
            }

            target.transform.position = lockState.position;
            target.transform.rotation = lockState.rotation;
        }

        public void UpdateTargetFromTime(float time)
        {
            WaypointPathState lockState = GetStateFromGlobalTime(positionalCurve.Evaluate(time), false);            
            UpdateTargetFromState(lockState);
        }

        public void SplitAt(int index, float time = .5f)
        {            
            StageCameraWaypoint[] newWaypoints = new StageCameraWaypoint[waypoints.Length+1];
            for(int i = 0; i <= index; i ++)
            {
                newWaypoints[i] = waypoints[i];
            }

            for(int i = index + 2; i < newWaypoints.Length; i ++)
            {
                newWaypoints[i] = waypoints[i - 1];
            }

            StageCameraWaypoint middlePoint = new StageCameraWaypoint();

            middlePoint.position = transform.InverseTransformPoint(GetPoint(index, time));
            middlePoint.rotation = GetRotation(index, time);
                
            middlePoint.ResetTangents();
            
            newWaypoints[index + 1] = middlePoint;
            
            waypoints = newWaypoints;
        }
    }

    
}