﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StageTools
{
    public class StageCamera : MonoBehaviour
    {
        public float zMax;


        public Vector3 localPosition { get { return m_LocalPosition; } set { m_LocalPosition = value; } }
        private Vector3 m_LocalPosition;

        public List<StageCameraState> states;

        private void Awake()
        {
            states = new List<StageCameraState>();
            localPosition = transform.localPosition;
        }

        public bool CapturePosition()
        {
            if (states != null && Application.isPlaying)
            {
                states.Add(new StageCameraState(transform.position, transform.rotation, Vector3.one));
                return true;
            }
            else
            {
                Debug.LogWarning("Capture is only possible in play mode");
                return false;
            }

        }

        public void ResetPosition()
        {
            localPosition = Vector3.zero;
            transform.localPosition = localPosition;
        }
    }

    public struct StageCameraState
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public StageCameraState(Vector3 _position, Quaternion _rotation, Vector3 _scale)
        {
            position = _position;
            rotation = _rotation;
            scale = _scale;
        }
    }
}