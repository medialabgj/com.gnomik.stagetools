# Stage tools

# Freeze tools
This tool allow you to save object transform position, rotation and scale while in play mode.
Add the component FreezableStageObject to any object that you want to keep track during play mode.
From the scene view you will be able to click over the object to switch to save mode or ignore mode.

If the object is in save mode when you exit the play mode, it its position, rotation and scale will be preserved.

# Stage camera tools
This tool allow to keep track of multiple camera position during play mode.
